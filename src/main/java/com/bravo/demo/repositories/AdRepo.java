package com.bravo.demo.repositories;

import com.bravo.demo.model.Ad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdRepo extends JpaRepository<Ad, Long> {

//    Ad findByTitle(String title);
}
