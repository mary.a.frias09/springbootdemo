package com.bravo.demo.repositories;

import com.bravo.demo.model.resaleItems;
import org.springframework.data.jpa.repository.JpaRepository;

public interface resaleItemRepo extends JpaRepository <resaleItems, Long> {

    resaleItems findByTitle(String title);



}
