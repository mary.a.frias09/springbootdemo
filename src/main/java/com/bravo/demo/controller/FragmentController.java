package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FragmentController {


    @GetMapping("/site")
    public String getLanding(){
        return "landingPage";
    }
}
