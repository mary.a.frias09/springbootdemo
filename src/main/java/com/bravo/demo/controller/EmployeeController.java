//package com.bravo.demo.controller;
//import com.bravo.demo.model.Employee;
//import com.bravo.demo.services.EmployeeService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@Controller
//public class EmployeeController {
//
//@Autowired  //used to inject object dependency
//    private EmployeeService employeeService;
//
////mapping to get the list of employee
////@GetMapping("/")
////    public String viewHomepage(Model model){
////    return findPaginated(1,"firstName", "asc", model);
////}
//
//@GetMapping("/showNewEmployeeForm")
//    public String showNewEmployeeForm(Model model){
////    create model to bind form data
//    Employee employee = new Employee();
//    model.addAttribute("employee", employee);
//    return "new_employee";
//}
//
//@PostMapping("/saveEmployee")
//    public String saveEmployee(@ModelAttribute("employee") Employee employee) {
////    save employee to database
//    employeeService.saveEmployee(employee);
//    return "redirect:/";
//}
// @GetMapping("/showFormForUpdate/{id}")
//    public String showFormForUpdate(@PathVariable(value = "id")long id, Model model){
////    get employee from service
//     Employee employee = employeeService.getEmployeeById(id);
////     set employee as a model attribute to pre populate the form
//     model.addAttribute("employee", employee);
//     return "update_employee";
// }
//
//
//// delete functionality
//    @GetMapping("/deleteEmployee/{id}")
//    public String deleteEmployee(@PathVariable(value = "id") long id){
////    call deleteEmployee method
//        this.employeeService.deleteEmployeeById(id);
//        return "redirect:/";
//    }
//
////
//    @GetMapping("/page/{pageNo}")
//    public String findPaginated(@PathVariable(value = "pageNo")int pageNo, @RequestParam("sortField")String sortField, @RequestParam("sortDir")String sortDir, Model model){
//    int pageSize = 5;
//    Page<Employee> page = employeeService.findPaginated(pageNo, pageSize, sortField, sortDir);
//        List<Employee>listEmployees= page.getContent();
//
//        model.addAttribute("currentPage", pageNo);
//        model.addAttribute("totalPages", page.getTotalPages());
//        model.addAttribute("totalItems", page.getTotalElements());
//        model.addAttribute("sort", sortField);
//        model.addAttribute("sortDir", sortDir);
//        model.addAttribute("reverseSortDir", sortDir.equals("asc")?"desc":"asc");
//        model.addAttribute("listEmployees", listEmployees);
//
//        return "index";
//    }
//
//}
//
//
//*******************************************

package com.bravo.demo.controller;
        import com.bravo.demo.model.Employee;
        import com.bravo.demo.services.EmployeeService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.data.domain.Page;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.web.bind.annotation.*;
        import java.util.List;

@Controller
public class EmployeeController {
    @Autowired//used to inject object dependency injecting employee service
    private EmployeeService employeeService;
//    mapping to get the list of employees

    @GetMapping("/")//goes to the index.html
    public String viewHomePage(Model model){
        return findPaginated(1,"firstName","asc", model);
    }

    @GetMapping("/showNewEmployeeForm")//links to show new employee th:href="@{/showNewEmployeeForm}
    public String showNewEmployeeForm(Model model){//creating a model class to add the employee to fro
        //create model to bind form data
        Employee employee = new Employee();//creating a new employee object
        model.addAttribute("employee",employee);//creating variable to send to the front
        return "new_employee";//return new_employee html
    }

    @PostMapping("/saveEmployee")//post new saved employee
    public String saveEmployee(@ModelAttribute("employee")Employee employee){//@ModelAttribute refers to a property of the Model object (the M in MVC ;) so let's say we have a form with a form backing object that is called "employee" Then you can have Spring MVC supply this object to a Controller method by using the @ModelAttribute annotation:
        //save employee to database
        employeeService.saveEmployee(employee);//attaching on model save employee service method of save employee
        return "redirect:/";//return redirect
    }


    @GetMapping("/showFormForUpdate/{id}")//path value id taking assigning id variable
    public String showFormForUpdate(@PathVariable(value="id")long id, Model model){//@PathVariable is a Spring annotation which indicates that a method parameter should be bound to a URI template variable. It has the following optional elements: name - name of the path variable to bind to.
        //get employee from service
        Employee employee = employeeService.getEmployeeById(id);//Calling employee service attaching method taking in id variable from method parameter
        //set employee as a model attribute to pre-populate the form
        model.addAttribute("employee", employee);//adding the employee variable saying that that employee is required
        return "update_employee";//returning update_employee.html
    }

    @GetMapping("/deleteEmployee/{id}")
    public String deleteEmployee(@PathVariable(value="id")long id){//creating delete employee method to match up to employee service
        //call delete employee method
        this.employeeService.deleteEmployeeById(id);//using this, this is a reference to the current object of employeeService attaching deleteEmployeeById method from service taking in id from the method parameter here in the controller
        return "redirect:/";
    }

    @GetMapping("/page/{pageNo}")//@RequestParam is a Spring annotation used to bind a web request parameter to a method parameter. It has the following optional elements:
    public String findPaginated(@PathVariable(value="pageNo") int pageNo, @RequestParam("sortField")String sortField,@RequestParam("sortDir") String sortDir, Model model){
        int pageSize = 5;    //creating int variable of pageSize assigning value of 5
        Page<Employee> page=employeeService.findPaginated(pageNo,pageSize,sortField,sortDir);//import org.springframework.data.domain.Page; page comes from spring framework
        // A page is a sublist of a list of objects. It allows gain information about the position of it in the containing entire list. Taking in pageNo,pageSize,sortField,sortDir attaching find paginated from service
        List<Employee> listEmployees=page.getContent();//creating employee list using get content
        model.addAttribute("currentPage", pageNo);//mode
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("sort", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc")?"desc":"asc");
        model.addAttribute("listEmployees", listEmployees);
        return "index";
    }
}







