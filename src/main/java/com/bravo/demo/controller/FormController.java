package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

//example of how to get data from the view
@Controller
public class FormController {   //file class

    @GetMapping("/join")
    public String showJoinForm(){

        return "join";
    }

    @PostMapping("/join")  //postmapping is used to post dat to the view
    public String joinCohort(@RequestParam(name = "cohort") String cohort, Model model){
//        @RequestParam used to read the form data and bind it automatically to the parameter variable present in the provided method
//        used "cohort" to represent example:
        model.addAttribute("cohort", "Welcome to " + cohort + "!");   //

        return "join";
    }

}
