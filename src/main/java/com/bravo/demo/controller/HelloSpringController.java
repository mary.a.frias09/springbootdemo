package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller  //this identifies communication to the url path to the spring MVC
public class HelloSpringController {

    @GetMapping("/hello")   //annotation to define what url map this method identifies with

    @ResponseBody           //defines what the response will be once the url is given
    public String hello(){  //the method called on the response body annotation
        return "Hello from SpringBoot!";    //return the data type of string message.

    }

    @GetMapping("/goodbye")
    @ResponseBody
    public String goodbye() {
        return "Goodbye from SpringBoot!";
    }

    //using requestmapping with path variable from spring framework
    //pathing variables - using {number} to pass in a variable inside url path
    @RequestMapping(path = "/increment/{number}")
    @ResponseBody
    public String sayFavNum(@PathVariable int number){
        return "Looks like your favorite number is : " + number;
    }

    @RequestMapping(path = "/increment/{number}", method = RequestMethod.GET)  //method class inside Spring Boot framework
    @ResponseBody  //annotation identifies the method will be the response
    public String addOne(@PathVariable int number) {  // passing in variable to identify to @requestmapping
        return number + "plus one is " + (number + 1) + "!";  //return statement to return on the method logic.
    }

}
