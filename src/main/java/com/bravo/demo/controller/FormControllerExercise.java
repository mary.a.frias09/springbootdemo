package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormControllerExercise {


    @GetMapping("/show-form")
    public String showTheForm() {

        return "company-form";
    }


    //mapping to process post information from the HTML
    @PostMapping("/process-form")
    public String processForm(
            @RequestParam(name = "firstName") String firstName,
            @RequestParam(name = "lastName") String lastName,
            @RequestParam(name = "city") String city,
            @RequestParam(name = "state") String state,
            @RequestParam(name = "email") String email,
            Model infoModel) {
        infoModel.addAttribute("name", "Name: " + firstName + " " + lastName);
        infoModel.addAttribute("hometown", "Hometown : " + city + " " + state);
        infoModel.addAttribute("contact", "Email : " + email);

        return "show-submit-form";
    }
}



//    Create a FormController
//        This class should have one method with a GetMapping for “/show-form”
//        Create a method to show the initial HTML form
//        *The name of your View should be company-form

//        Create a View named company-form

//        In your FormController
//        Create a controller method to process the HTML form
//        Assign the url “/process-form”
//        Where you will see the information that was submitted
//        This method should return the name of your View that contains the submitted information
//        *The name of this new View is “show-form-submit”

//        Create a new View
//        Create a View named show-form-submit
//        In this View, you are displaying all the information that the user submitted

//        BONUS:
//        Make your views user-friendly!
//        Meaning add some messages that indicates the user what each webpage is.
//        Example. "Welcome to Alpha! Please complete the form"
