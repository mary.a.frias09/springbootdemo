package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MathController {

    @GetMapping(path = "/add/{num1}/and/{num2}")
    @ResponseBody
    public int addNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 + num2;
    }


    //subtract method
    @GetMapping(path = "/subtract/{num1}/and/{num2}")
    @ResponseBody
    public int subtractNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 - num2;
    }


    //multiply method
    @GetMapping(path = "/multiply/{num1}/and/{num2}")
    @ResponseBody
    public int multiplyNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 * num2;
    }



    //divide method
    @GetMapping(path = "/divide/{num1}/and/{num2}")
    @ResponseBody
    public int divideNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 / num2;
    }



    //subtract method
//    @GetMapping(path = "/subtract/{num1}/and/{num2}")
//    @ResponseBody
//    public int subNumbers(@PathVariable int num1, @PathVariable int num2){
//        return num1 - num2;
//    }


}




//
//   Create a MathController class
//        This controller should have a request for several routes
//        that runs basic math operations
//        And produce the result of the arithmetic
