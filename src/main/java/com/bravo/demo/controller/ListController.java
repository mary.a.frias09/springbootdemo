package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ListController {
//        How to iterate over a list and display to the view(HTML)

    @GetMapping("/hello-names")
    public String helloNames(Model model) {     //pass a variable create a model object from spring framework

        List<String> names = new ArrayList<>();
        names.add("Adrian");
        names.add("MaryAnn");
        names.add("Sandra");
        names.add("Eric");
        names.add("Jonathan");
        names.add("Henry");

        model.addAttribute("names", names);

        return "List";
    }

}
