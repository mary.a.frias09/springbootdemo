package com.bravo.demo.controller;
import com.bravo.demo.model.User;
import com.bravo.demo.repositories.UserRepo;
import com.bravo.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class UserController {
//    2 variables provide access to jpa api through repo variable
    private UserRepo users;
    private PasswordEncoder passwordEncoder;

    public UserController(UserRepo users, PasswordEncoder passwordEncoder) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }

//    sign up form
//    @GetMapping("/")
//    public String landing() {
//        return "Users/sign-up";
//    }

//    sign-up form calling model to send to front end
    @GetMapping("sign-up")
    public String showSignUpForm(Model model) {
        model.addAttribute("user", new User());
        return "Users/sign-up";
    }

//    saving user
    @PostMapping("/sign-up")
    public String saveUser(@ModelAttribute User user) {
        String hash = passwordEncoder.encode(user.getPassword());  //assigning hash to passwordEncoder and attaching .getPassword method onto user to get the password
        boolean debug = passwordEncoder.matches(user.getPassword(),hash);
        user.setPassword(hash);
        users.save(user);  //saving user and user info
        return "redirect:/showNewEmployeeForm";  //redirect if credentials are good go to employeeForm
    }






}





//
//@Controller
//public class UserController {
//
//@Autowired
//UserRepository userRepo;
//@RequestMapping("/users")
//
//    public String home(Model model){
//
//    model.addAttribute("users", userRepo.findAll());
//    return "users";
//}
//
//
//
//}
