package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@Controller
public class CasinoRoll {

    @GetMapping("/casino-roll")
    public String guessRoll() {
        return "guess-roll";
    }

    @GetMapping("/casino-roll/{number}")
    public String userGuessNum(@PathVariable int number, Model model) {
        int dice = (int) (Math.random() * ((5) + 1));  //creating int dice object, assigning Math.random algorithm.
        String message;  //string var called message
        if (dice == number) {  //if statement to test math.random algorithm against int num variable
            message = "You guessed correctly!";
        } else {
            message = "Sorry, try again!";
        }
        model.addAttribute("message", message);  //model class assigning the var we want to pass to the html and our message

        return "guess-roll";
    }


}
















//    Create a page for “/casino-roll”
//        This page should ask the user to guess a number.
//        There should be links on the for numbers 1 through 6 - this should make a GET request to
//        “/casino-roll/num” where num is the number guessed.
//        This page should display
//        1. a random number (the dice roll)
//        2. The number the user guessed
//        3. A message based on whether or not they guessed the number correctly
//        BONUS
//        “Roll” a series of dice on each page load, as opposed to an individual die.
//        Show all the rolls to the user, and display the number of corrected guesses.
